(function ($, Drupal, drupalSettings, once) {

  let config = {
    chart: {
      height: (3 / 4 * 100) + '%',
      type: 'organization',
      inverted: true
    },
    title: {
      useHTML: true,
      text: Drupal.t('Organization Chart')
    },
    accessibility: {
      point: {
        descriptionFormat: '{add index 1}. {toNode.name}' +
          '{#if (ne toNode.name toNode.id)}, {toNode.id}{/if}, ' +
          'reports to {fromNode.id}'
      }
    },
    series: [{
      type: 'organization',
      keys: ['from', 'to'],
      colorByPoint: false,
      color: '#007ad0',
      dataLabels: {
        color: 'white'
      },
      borderColor: 'white',
      nodeWidth: 'auto',
    }],
    tooltip: {
      outside: true,
      formatter: function () {
        if (this.point.tooltip) {
          return this.point.tooltip;
        }
        return this.point.name
      }
    },
    exporting: {
      allowHTML: true,
      sourceWidth: 800,
      sourceHeight: 600
    }
  };
  Drupal.behaviors.organization = {
    attach: function attach(context) {
      $(once('organization', ".views-view-organization", context))
        .each(function () {
          let id = $(this).attr('id');
          config.series[0].levels = drupalSettings[id].levels;
          config.series[0].nodes = drupalSettings[id].nodes;
          config.series[0].data = drupalSettings[id].data;
          config.series[0].name = drupalSettings[id].title;
          config.title.text = drupalSettings[id].title;
          config.chart.inverted = drupalSettings[id].inverted;
          if (drupalSettings[id].height) {
            config.chart.height = drupalSettings[id].height;
          }
          if (drupalSettings[id].width) {
            config.chart.width = drupalSettings[id].width;
          }
          if (drupalSettings[id].subtitle) {
            config.subtitle = {
              useHTML: true,
              text: drupalSettings[id].subtitle,
            };
          }
          if(drupalSettings[id].tooltip_field === '') {
            delete config.tooltip.formatter;
          }
          Highcharts.chart(id, config);
        });
    }
  };
}(jQuery, Drupal, drupalSettings, once));
